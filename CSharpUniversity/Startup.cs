﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CSharpUniversity.Startup))]
namespace CSharpUniversity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
