﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSharpUniversity.DAL.EF;

namespace CSharpUniversity.Controllers.Settings
{
    public class Weekly_ActivityController : Controller
    {
        private cSharpEntities db = new cSharpEntities();

        // GET: Weekly_Activity
        public ActionResult Index()
        {
            var weekly_Activity = db.Weekly_Activity.Include(w => w.Activity).Include(w => w.Term_Period);
            return View(weekly_Activity.ToList());
        }

        // GET: Weekly_Activity/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Weekly_Activity weekly_Activity = db.Weekly_Activity.Find(id);
            if (weekly_Activity == null)
            {
                return HttpNotFound();
            }
            return View(weekly_Activity);
        }

        // GET: Weekly_Activity/Create
        public ActionResult Create()
        {
            ViewBag.ActivityId = new SelectList(db.Activities, "ActivityId", "Name");
            ViewBag.WeekId = new SelectList(db.Term_Period, "Week", "Week");
            return View();
        }

        // POST: Weekly_Activity/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "WeekId,ActivityId,WeeklyActivityId")] Weekly_Activity weekly_Activity)
        {
            if (ModelState.IsValid)
            {
                db.Weekly_Activity.Add(weekly_Activity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ActivityId = new SelectList(db.Activities, "ActivityId", "Name", weekly_Activity.ActivityId);
            ViewBag.WeekId = new SelectList(db.Term_Period, "Week", "Week", weekly_Activity.WeekId);
            return View(weekly_Activity);
        }

        // GET: Weekly_Activity/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Weekly_Activity weekly_Activity = db.Weekly_Activity.Find(id);
            if (weekly_Activity == null)
            {
                return HttpNotFound();
            }
            ViewBag.ActivityId = new SelectList(db.Activities, "ActivityId", "Name", weekly_Activity.ActivityId);
            ViewBag.WeekId = new SelectList(db.Term_Period, "Week", "Week", weekly_Activity.WeekId);
            return View(weekly_Activity);
        }

        // POST: Weekly_Activity/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "WeekId,ActivityId,WeeklyActivityId")] Weekly_Activity weekly_Activity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(weekly_Activity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ActivityId = new SelectList(db.Activities, "ActivityId", "Name", weekly_Activity.ActivityId);
            ViewBag.WeekId = new SelectList(db.Term_Period, "Week", "Week", weekly_Activity.WeekId);
            return View(weekly_Activity);
        }

        // GET: Weekly_Activity/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Weekly_Activity weekly_Activity = db.Weekly_Activity.Find(id);
            if (weekly_Activity == null)
            {
                return HttpNotFound();
            }
            return View(weekly_Activity);
        }

        // POST: Weekly_Activity/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Weekly_Activity weekly_Activity = db.Weekly_Activity.Find(id);
            db.Weekly_Activity.Remove(weekly_Activity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
