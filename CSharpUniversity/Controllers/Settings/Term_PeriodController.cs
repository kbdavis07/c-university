﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSharpUniversity.DAL.EF;

namespace CSharpUniversity.Controllers.Settings
{
    public class Term_PeriodController : Controller
    {
        private cSharpEntities db = new cSharpEntities();

        // GET: Term_Period
        public ActionResult Index()
        {
            return View(db.Term_Period.ToList());
        }

        // GET: Term_Period/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Term_Period term_Period = db.Term_Period.Find(id);
            if (term_Period == null)
            {
                return HttpNotFound();
            }
            return View(term_Period);
        }

        // GET: Term_Period/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Term_Period/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Week,Start,End")] Term_Period term_Period)
        {
            if (ModelState.IsValid)
            {
                db.Term_Period.Add(term_Period);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(term_Period);
        }

        // GET: Term_Period/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Term_Period term_Period = db.Term_Period.Find(id);
            if (term_Period == null)
            {
                return HttpNotFound();
            }
            return View(term_Period);
        }

        // POST: Term_Period/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Week,Start,End")] Term_Period term_Period)
        {
            if (ModelState.IsValid)
            {
                db.Entry(term_Period).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(term_Period);
        }

        // GET: Term_Period/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Term_Period term_Period = db.Term_Period.Find(id);
            if (term_Period == null)
            {
                return HttpNotFound();
            }
            return View(term_Period);
        }

        // POST: Term_Period/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Term_Period term_Period = db.Term_Period.Find(id);
            db.Term_Period.Remove(term_Period);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
