﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSharpUniversity.DAL.EF;

namespace CSharpUniversity.Controllers.Settings
{
    public class Letter_PointsController : Controller
    {
        private cSharpEntities db = new cSharpEntities();

        // GET: Letter_Points
        public ActionResult Index()
        {
            return View(db.Letter_Points.ToList());
        }

        // GET: Letter_Points/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Letter_Points letter_Points = db.Letter_Points.Find(id);
            if (letter_Points == null)
            {
                return HttpNotFound();
            }
            return View(letter_Points);
        }

        // GET: Letter_Points/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Letter_Points/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LetterPointsId,Min,Max,Letter,Points")] Letter_Points letter_Points)
        {
            if (ModelState.IsValid)
            {
                db.Letter_Points.Add(letter_Points);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(letter_Points);
        }

        // GET: Letter_Points/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Letter_Points letter_Points = db.Letter_Points.Find(id);
            if (letter_Points == null)
            {
                return HttpNotFound();
            }
            return View(letter_Points);
        }

        // POST: Letter_Points/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LetterPointsId,Min,Max,Letter,Points")] Letter_Points letter_Points)
        {
            if (ModelState.IsValid)
            {
                db.Entry(letter_Points).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(letter_Points);
        }

        // GET: Letter_Points/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Letter_Points letter_Points = db.Letter_Points.Find(id);
            if (letter_Points == null)
            {
                return HttpNotFound();
            }
            return View(letter_Points);
        }

        // POST: Letter_Points/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Letter_Points letter_Points = db.Letter_Points.Find(id);
            db.Letter_Points.Remove(letter_Points);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
