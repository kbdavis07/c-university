﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CSharpUniversity.DAL.EF;

namespace CSharpUniversity.Controllers.Settings
{
    public class Activity_TypesController : Controller
    {
        private cSharpEntities db = new cSharpEntities();

        // GET: Activity_Types
        public ActionResult Index()
        {
            return View(db.Activity_Types.ToList());
        }

        // GET: Activity_Types/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity_Types activity_Types = db.Activity_Types.Find(id);
            if (activity_Types == null)
            {
                return HttpNotFound();
            }
            return View(activity_Types);
        }

        // GET: Activity_Types/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Activity_Types/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TypeId,Type,Weight")] Activity_Types activity_Types)
        {
            if (ModelState.IsValid)
            {
                db.Activity_Types.Add(activity_Types);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(activity_Types);
        }

        // GET: Activity_Types/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity_Types activity_Types = db.Activity_Types.Find(id);
            if (activity_Types == null)
            {
                return HttpNotFound();
            }
            return View(activity_Types);
        }

        // POST: Activity_Types/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TypeId,Type,Weight")] Activity_Types activity_Types)
        {
            if (ModelState.IsValid)
            {
                db.Entry(activity_Types).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(activity_Types);
        }

        // GET: Activity_Types/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity_Types activity_Types = db.Activity_Types.Find(id);
            if (activity_Types == null)
            {
                return HttpNotFound();
            }
            return View(activity_Types);
        }

        // POST: Activity_Types/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Activity_Types activity_Types = db.Activity_Types.Find(id);
            db.Activity_Types.Remove(activity_Types);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
