//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CSharpUniversity.DAL.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Term_Period
    {
        public Term_Period()
        {
            this.Weekly_Activity = new HashSet<Weekly_Activity>();
        }
    
        public int Week { get; set; }
        public Nullable<System.DateTime> Start { get; set; }
        public Nullable<System.DateTime> End { get; set; }
    
        public virtual ICollection<Weekly_Activity> Weekly_Activity { get; set; }
    }
}
